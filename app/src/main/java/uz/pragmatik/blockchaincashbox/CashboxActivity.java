package uz.pragmatik.blockchaincashbox;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import uz.pragmatik.blockchaincashbox.database.manager.ProductManager;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ProductImpl;
import uz.pragmatik.blockchaincashbox.database.models.Product;
import uz.pragmatik.blockchaincashbox.database.models.Settings;
import uz.pragmatik.blockchaincashbox.fragments.HistoryFragment;
import uz.pragmatik.blockchaincashbox.fragments.ProductsFragment;
import uz.pragmatik.blockchaincashbox.fragments.SellingFragment;
import uz.pragmatik.blockchaincashbox.fragments.SettingsFragment;

public class CashboxActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

  TextView tvTitle;
  @Override protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cashbox);
    Toolbar toolbar = findViewById(R.id.toolbar);
    setSupportActionBar(toolbar);
    getSupportActionBar().setDisplayShowTitleEnabled(false);


    tvTitle = findViewById(R.id.tv_toolbar_title);

    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    ActionBarDrawerToggle toggle =
        new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
    drawer.addDrawerListener(toggle);
    toggle.syncState();

    NavigationView navigationView = findViewById(R.id.nav_view);
    navigationView.setNavigationItemSelectedListener(this);
    navigationView.setCheckedItem(R.id.nav_selling);
    showFragment(0);
    ProductManager manager = new ProductImpl(this);
    List<Product> list = manager.getAll();
    if (list == null || list.size()==0){
      Product product = new Product();
      product.setName("Coca-Cola 1.5Л");
      product.setPrice(new BigDecimal(6500));
      manager.save(product);

      product = new Product();
      product.setName("Кол. полукопч. SHERIN Танлаган Посольс.");
      product.setPrice(new BigDecimal(20800));
      manager.save(product);

      product = new Product();
      product.setName("Сосиски SHERIN Венские длин. вакуум");
      product.setPrice(new BigDecimal(15600));
      manager.save(product);

      product = new Product();
      product.setName("Сок \"Сочная долина\" Абрикос 200мл");
      product.setPrice(new BigDecimal(1800));
      manager.save(product);

      product = new Product();
      product.setName("Нектар Viko Экзотик 1л");
      product.setPrice(new BigDecimal(6700));
      manager.save(product);

      product = new Product();
      product.setName("Сок Rich Апельсин 1л");
      product.setPrice(new BigDecimal(19000));
      manager.save(product);

      product = new Product();
      product.setName("Энергет.напиток Plus 18+ Ж/Б 500мл");
      product.setPrice(new BigDecimal(6800));
      manager.save(product);

      product = new Product();
      product.setName("МАСЛО APPETITO ИРЛАНДСКОЕ_72,5% 500ГР");
      product.setPrice(new BigDecimal(14000));
      manager.save(product);

      product = new Product();
      product.setName("Кефир Камилка 1% 500гр");
      product.setPrice(new BigDecimal(3400));
      manager.save(product);

      product = new Product();
      product.setName("Слив. масло Appetito Топлёное 500г");
      product.setPrice(new BigDecimal(11300));
      manager.save(product);

      product = new Product();
      product.setName("Вафли \"Яшкино\"Шоколадное200г");
      product.setPrice(new BigDecimal(8500));
      manager.save(product);

      product = new Product();
      product.setName("Печенье \"Яшкино\"Апельсин 137г");
      product.setPrice(new BigDecimal(7500));
      manager.save(product);
    }
    Settings settings = new Settings(getApplicationContext());
    if (settings.getCompanyAddress().isEmpty()) {
      settings.setCompanyAddress("Mirobod tumani, Shahrisabz ko'chasi, 23 uy.");
    }
    if (settings.getCompanyInn().isEmpty()) {
      settings.setCompanyInn("123456789");
    }
    if (settings.getCompanyName().isEmpty()) {
      settings.setCompanyName("Barakat Savdo");
    }
    if (settings.getServerIpAddress().isEmpty()) {
      settings.setServerIpAddress("http://192.168.1.174:7545");
    }
  }

  @Override public void onBackPressed() {
    DrawerLayout drawer = findViewById(R.id.drawer_layout);
    if (drawer.isDrawerOpen(GravityCompat.START)) {
      drawer.closeDrawer(GravityCompat.START);
    } else {
      super.onBackPressed();
    }
  }


  private void showFragment(int index) {
    FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
    switch (index)
    {
      case 0:
        fragmentTransaction.replace(R.id.frame, new SellingFragment());
        tvTitle.setText(getResources().getString(R.string.selling));
        break;
      case 1:
        fragmentTransaction.replace(R.id.frame, new HistoryFragment());
        tvTitle.setText(getResources().getString(R.string.history));
        break;
      case 2:
        fragmentTransaction.replace(R.id.frame, new ProductsFragment());
        tvTitle.setText(getResources().getString(R.string.products));
        break;
      case 3:
        fragmentTransaction.replace(R.id.frame, new SettingsFragment());
        tvTitle.setText(getResources().getString(R.string.settings));
        break;
      default:
        fragmentTransaction.replace(R.id.frame, new SellingFragment());
        tvTitle.setText(getResources().getString(R.string.selling));
        break;
    }
    fragmentTransaction.commit();

  }


  @Override public boolean onNavigationItemSelected(MenuItem item) {
    // Handle navigation view item clicks here.
    int id = item.getItemId();
    switch (id)
    {
      case R.id.nav_selling:
        showFragment(0);
        break;
      case R.id.nav_history:
        showFragment(1);
        break;
      case R.id.nav_products:
      showFragment(2);
        break;
      case R.id.nav_settings:
        showFragment(3);
        break;
      default:
        showFragment(0);
        break;
    }


    DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
    drawer.closeDrawer(GravityCompat.START);
    return true;
  }
}
