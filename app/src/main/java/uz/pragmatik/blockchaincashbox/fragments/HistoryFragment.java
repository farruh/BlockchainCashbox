package uz.pragmatik.blockchaincashbox.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.adapters.HistoryExpandableListAdapter;
import uz.pragmatik.blockchaincashbox.database.ChequeHelper;
import uz.pragmatik.blockchaincashbox.database.models.Cheque;
import uz.pragmatik.blockchaincashbox.database.models.ChequeHeader;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;

/**
 * Created by QarakenBacho on 17.04.2018.
 */

public class HistoryFragment extends Fragment {

    HistoryExpandableListAdapter listAdapter;
    ExpandableListView expListView;
    List<ChequeHeader> listDataHeader;
    HashMap<ChequeHeader, List<ChequeProduct>> listDataChild;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);

        // get the listview
        expListView = view.findViewById(R.id.lvExp);

        // preparing list data
        prepareListData();

        listAdapter = new HistoryExpandableListAdapter(getActivity(), listDataHeader, listDataChild);

        // setting list adapter
        expListView.setAdapter(listAdapter);
        return view;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<ChequeHeader>();
        listDataChild = new HashMap<ChequeHeader, List<ChequeProduct>>();
        ChequeHelper chequeHelper = new ChequeHelper(getActivity());
        for(Cheque cheque : chequeHelper.getAllCheque()){
            ChequeHeader header = new ChequeHeader();
            header.setChequeLabel("Чек №" + cheque.getId());
            header.setChequeTotalSum(cheque.getTotalSum().toString());
            if (cheque.getTotalCashSum()!=null && !cheque.getTotalCashSum().equals(BigDecimal.ZERO)){
                header.setPaymentType(getActivity().getString(R.string.cash));
            } else {
                header.setPaymentType(getActivity().getString(R.string.card));
            }
            listDataHeader.add(header);
            listDataChild.put(listDataHeader.get(listDataHeader.size()-1), cheque.getProductListFromDb());
        }
    }
}

