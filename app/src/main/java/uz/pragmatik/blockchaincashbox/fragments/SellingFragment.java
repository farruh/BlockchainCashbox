package uz.pragmatik.blockchaincashbox.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialog;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import uz.pragmatik.blockchaincashbox.PaymentActivity;
import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.adapters.BasketAdapter;
import uz.pragmatik.blockchaincashbox.adapters.ProductsAdapter;
import uz.pragmatik.blockchaincashbox.database.manager.ProductManager;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ProductImpl;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.database.models.Product;
import uz.pragmatik.blockchaincashbox.utils.RecyclerItemListener;

/**
 * Created by QarakenBacho on 17.04.2018.
 */

public class SellingFragment extends Fragment implements View.OnClickListener {



    ProductManager manager;
    List<Product> productList;
    public static List<ChequeProduct> chequeProductsList;
    private RecyclerView rvProducts;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private Button btnPay;
    private BigDecimal totalSum = BigDecimal.ZERO;
    private TextView tvTotal;
    @Override
    public void onResume() {
        super.onResume();
        manager = new ProductImpl(getActivity());
        productList = manager.getAll();

        // specify an adapter (see also next example)
        mAdapter = new ProductsAdapter(productList, getActivity());
        rvProducts.setAdapter(mAdapter);


    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_selling, container, false);
        btnPay = view.findViewById(R.id.btn_pay);
        btnPay.setEnabled(false);
        btnPay.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
        btnPay.setOnClickListener(this);
        tvTotal = view.findViewById(R.id.tv_total);
        tvTotal.setText("0.00");


        rvProducts = view.findViewById(R.id.rv_products);
        rvProducts.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(getActivity());
        rvProducts.setLayoutManager(mLayoutManager);
        chequeProductsList = new ArrayList<>();

        rvProducts.addOnItemTouchListener(new RecyclerItemListener(getActivity(), rvProducts,
                new RecyclerItemListener.RecyclerTouchListener() {
                    public void onClickItem(View v, int position) {

                        ChequeProduct chequeProduct = new ChequeProduct();
                        chequeProduct.setName(productList.get(position).getName());
                        chequeProduct.setPrice(productList.get(position).getPrice());
                        chequeProduct.setProductId(productList.get(position).getId());
                        chequeProduct.setQuantity(BigDecimal.ONE);
                        chequeProduct.setTotalSum(productList.get(position).getPrice());
                        boolean b = false;
                        if (chequeProductsList != null && chequeProductsList.size()>0){
                            for (ChequeProduct product : chequeProductsList){
                                if (product.getProductId()==chequeProduct.getProductId()){
                                    product.setQuantity(product.getQuantity().add(BigDecimal.ONE));
                                    product.setTotalSum(product.getQuantity().multiply(product.getPrice()));
                                    b = true;
                                }
                            }
                        }
                        if (!b){
                            chequeProductsList.add(chequeProduct);
                        }

                        totalSum = BigDecimal.ZERO;
                        for (ChequeProduct product: chequeProductsList){
                            totalSum = totalSum.add(product.getPrice().multiply(product.getQuantity()));
                        }
                        tvTotal.setText(totalSum.toString());
                        btnPay.setEnabled(true);
                        btnPay.getBackground().setColorFilter(null);
                    }

                    public void onLongClickItem(View v, int position) {
                        System.out.println("On Long Click Item interface");
                    }
                }));


        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_sales_receipt:
                if (chequeProductsList != null && chequeProductsList.size()>0){
                    showReciept();
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showReciept() {

        View view = getLayoutInflater().inflate(R.layout.fragment_bottom_sheet_reciept, null);

        final BottomSheetDialog dialog = new BottomSheetDialog(getActivity());
        dialog.setContentView(view);

        dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                totalSum = BigDecimal.ZERO;
                for (ChequeProduct product: chequeProductsList){
                    totalSum = totalSum.add(product.getPrice().multiply(product.getQuantity()));
                }
                tvTotal.setText(totalSum.toString());
                if (totalSum == BigDecimal.ZERO){
                    btnPay.setEnabled(false);
                    btnPay.getBackground().setColorFilter(Color.GRAY, PorterDuff.Mode.MULTIPLY);
                }

            }
        });
        TextView tvClose = view.findViewById(R.id.tv_close);
        tvClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        TextView tvDelete = view.findViewById(R.id.tv_delete);
        tvDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                chequeProductsList = new ArrayList<>();

                dialog.dismiss();
            }
        });
        RecyclerView rvBacket = view.findViewById(R.id.rv_busket);
        rvBacket.setHasFixedSize(true);

        RecyclerView.LayoutManager bLayoutManager = new LinearLayoutManager(getActivity());
        rvBacket.setLayoutManager(bLayoutManager);


        // specify an adapter (see also next example)
        RecyclerView.Adapter bAdapter = new BasketAdapter(chequeProductsList, getActivity());
        rvBacket.setAdapter(bAdapter);

        dialog.show();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_selling, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case R.id.btn_pay:
                Intent intentPay = new Intent(getActivity(), PaymentActivity.class);
                startActivity(intentPay);
            break;
        }
    }

}