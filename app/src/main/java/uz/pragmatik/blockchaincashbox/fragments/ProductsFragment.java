package uz.pragmatik.blockchaincashbox.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import uz.pragmatik.blockchaincashbox.AddProductActivity;
import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.adapters.ProductsAdapter;
import uz.pragmatik.blockchaincashbox.database.manager.ProductManager;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ProductImpl;
import uz.pragmatik.blockchaincashbox.database.models.Product;
import uz.pragmatik.blockchaincashbox.utils.Constants;
import uz.pragmatik.blockchaincashbox.utils.RecyclerItemListener;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class ProductsFragment extends Fragment {

    ProductManager manager;
    List<Product> productList;
    private RecyclerView rvProducts;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    @Override
    public void onResume() {
        super.onResume();
        manager = new ProductImpl(getActivity());
        productList = manager.getAll();

        // specify an adapter (see also next example)
        mAdapter = new ProductsAdapter(productList, getActivity());
        rvProducts.setAdapter(mAdapter);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_products, container, false);

        rvProducts = view.findViewById(R.id.rv_products);
        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        rvProducts.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        rvProducts.setLayoutManager(mLayoutManager);



        rvProducts.addOnItemTouchListener(new RecyclerItemListener(getActivity(), rvProducts,
                new RecyclerItemListener.RecyclerTouchListener() {
                    public void onClickItem(View v, int position) {
                        Intent editIntent = new Intent(getActivity(), AddProductActivity.class);
                        editIntent.putExtra(Constants.PRODUCT_ID, productList.get(position).getId());
                        startActivity(editIntent);
                    }

                    public void onLongClickItem(View v, int position) {
                        System.out.println("On Long Click Item interface");
                    }
                }));

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_add_product:
                Intent newProductIntent = new Intent(getActivity(), AddProductActivity.class);
                startActivity(newProductIntent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_products, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


}