package uz.pragmatik.blockchaincashbox.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.database.models.Settings;


/**
 * Created by QarakenBacho on 17.04.2018.
 */

public class SettingsFragment extends Fragment {

    private EditText edit_company_inn;
    private EditText edit_company_name;
    private EditText edit_company_address;
    private EditText edit_server_ip_address;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        Settings settings = new Settings(getActivity());
        edit_company_address = view.findViewById(R.id.edit_company_address);
        edit_company_inn = view.findViewById(R.id.edit_company_inn);
        edit_company_name = view.findViewById(R.id.edit_company_name);
        edit_server_ip_address = view.findViewById(R.id.edit_server_ip);

        edit_company_name.setText(settings.getCompanyName());
        edit_company_inn.setText(settings.getCompanyInn());
        edit_company_address.setText(settings.getCompanyAddress());
        edit_server_ip_address.setText(settings.getServerIpAddress());
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                Settings settings = new Settings(getActivity());
                settings.setCompanyAddress(edit_company_address.getText().toString());
                settings.setCompanyInn(edit_company_inn.getText().toString());
                settings.setCompanyName(edit_company_name.getText().toString());
                settings.setServerIpAddress(edit_server_ip_address.getText().toString());

                Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.saved), Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_save, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

}

