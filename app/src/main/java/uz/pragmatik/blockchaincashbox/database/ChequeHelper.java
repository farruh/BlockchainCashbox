package uz.pragmatik.blockchaincashbox.database;

import android.content.Context;

import android.util.Log;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import java.util.concurrent.Callable;
import org.web3j.utils.Async;
import uz.pragmatik.blockchaincashbox.blockchain.remote.Web3jService;
import uz.pragmatik.blockchaincashbox.database.manager.ChequeManager;
import uz.pragmatik.blockchaincashbox.database.manager.ChequeProductManager;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ChequeImpl;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ChequeProductImpl;
import uz.pragmatik.blockchaincashbox.database.models.Cheque;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.database.models.Settings;

/**
 * Created by destbest on 18.04.2018.
 */

public class ChequeHelper {

  private Context context;
  private ChequeManager chequeManager;
  private ChequeProductManager chequeProductManager;

  public ChequeHelper(Context context) {
    this.context = context;
    chequeManager = new ChequeImpl(context);
    chequeProductManager = new ChequeProductImpl(context);
  }

  public void setCheque(final Cheque cheque) {
    final Settings settings = new Settings(context);
    cheque.setOperationDate(new Date());
    cheque.setCompanyAddress(settings.getCompanyAddress());
    cheque.setCompanyInn(settings.getCompanyInn());
    cheque.setCompanyName(settings.getCompanyName());
    chequeManager.save(cheque);
    for (ChequeProduct chequeProduct : cheque.getProductList()) {
      chequeProduct.setCheque(cheque);
      chequeProductManager.save(chequeProduct);
    }
    cheque.setChequeNumber(cheque.getId());
    cheque.setChequeSign((long) cheque.getId().hashCode());
    if (cheque.getTotalCashSum() == null) {
      cheque.setTotalCashSum(BigDecimal.ZERO);
    }
    if (cheque.getTotalEcashSum() == null) {
      cheque.setTotalEcashSum(BigDecimal.ZERO);
    }
    final String serverIpAddress = settings.getServerIpAddress();
    Log.d("network", "Send cheques to blockchain network "+serverIpAddress);
    Async.run(new Callable<Void>() {
      @Override public Void call() throws Exception {
        new Web3jService(serverIpAddress).sendCheque(cheque);
        return null;
      }
    });
  }

  public List<Cheque> getAllCheque() {
    return chequeManager.getAll();
  }

  public Cheque getChequeById(Long id) {
    return chequeManager.get(id);
  }
}
