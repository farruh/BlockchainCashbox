package uz.pragmatik.blockchaincashbox.database.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;


import uz.pragmatik.blockchaincashbox.database.models.Cheque;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.database.models.Product;

/**
 * Created by destbest on 18.04.2018.
 */

public class OrmLiteHelper extends OrmLiteSqliteOpenHelper {

    private static final String _DATABASE_NAME = "smartpos.db";
    private static final int _DATABASE_VERSION = 1;

    public OrmLiteHelper(Context context) {
        super(context, _DATABASE_NAME, null, _DATABASE_VERSION);
    }

    public void initDatabase() {
        createTables(getWritableDatabase(), getConnectionSource());
    }

    private void createTables(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        try {
            TableUtils.createTableIfNotExists(connectionSource, Product.class);
            TableUtils.createTableIfNotExists(connectionSource, Cheque.class);
            TableUtils.createTableIfNotExists(connectionSource, ChequeProduct.class);
        } catch (Exception e) {
            Log.e("SQLError", "Error during creating tables", e);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource) {
        createTables(sqLiteDatabase, connectionSource);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, ConnectionSource connectionSource, int i, int i1) {
        try {
            TableUtils.dropTable(connectionSource, Cheque.class, true);
            TableUtils.dropTable(connectionSource, ChequeProduct.class, true);
            TableUtils.dropTable(connectionSource, Product.class, true);
        } catch (Exception e) {
            Log.e("SQLError", "Error during dropping tables", e);
        }
    }
}
