package uz.pragmatik.blockchaincashbox.database.manager;

import java.util.List;

/**
 * @author Farruh Tashbulov
 */

public interface BaseManager<T,P> {
    public List<T> getAll();
    public T get(P pk);

    public void save(T object);
    public void delete(P pk);

    public void clearAll();
}
