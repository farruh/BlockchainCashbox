package uz.pragmatik.blockchaincashbox.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by destbest on 18.04.2018.
 */

@DatabaseTable(tableName = "cheque_product")
public class ChequeProduct implements Serializable {

    private static final long serialVersionUID = 1L;
    @DatabaseField(columnName = "id", generatedId = true)
    private Long id;

    private Long productId;

    @DatabaseField(columnName = "name")
    private String name;
    @DatabaseField(columnName = "price")
    private BigDecimal price;
    @DatabaseField(columnName = "quantity")
    private BigDecimal quantity;
    @DatabaseField(columnName = "total_sum")
    private BigDecimal totalSum;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Cheque cheque;

    public ChequeProduct() {
    }

    public ChequeProduct(Long id, String name, BigDecimal price, BigDecimal quantity, BigDecimal totalSum, Cheque cheque) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.totalSum = totalSum;
        this.cheque = cheque;
    }

    public Long getProductId() {
        return productId;
    }

    public void setProductId(Long productId) {
        this.productId = productId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public Cheque getCheque() {
        return cheque;
    }

    public void setCheque(Cheque cheque) {
        this.cheque = cheque;
    }
}
