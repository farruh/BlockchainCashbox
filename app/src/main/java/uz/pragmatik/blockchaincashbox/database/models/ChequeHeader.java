package uz.pragmatik.blockchaincashbox.database.models;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class ChequeHeader {
    private String chequeLabel;
    private String chequeTotalSum;
    private String paymentType;

    public String getChequeLabel() {
        return chequeLabel;
    }

    public void setChequeLabel(String chequeLabel) {
        this.chequeLabel = chequeLabel;
    }

    public String getChequeTotalSum() {
        return chequeTotalSum;
    }

    public void setChequeTotalSum(String chequeTotalSum) {
        this.chequeTotalSum = chequeTotalSum;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }
}
