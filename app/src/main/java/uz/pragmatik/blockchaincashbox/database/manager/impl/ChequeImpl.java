package uz.pragmatik.blockchaincashbox.database.manager.impl;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import uz.pragmatik.blockchaincashbox.database.manager.ChequeManager;
import uz.pragmatik.blockchaincashbox.database.models.Cheque;

/**
 * Created by destbest on 18.04.2018.
 */

public class ChequeImpl extends BaseManagerImpl<Cheque, Long> implements ChequeManager{

    @Override
    public List<Cheque> getAll() {
        List<Cheque> list = null;
        try {
            list = getDao().queryForAll();
        } catch (SQLException e) {
            Log.e("SQLException", "Error during get all Cheque", e);
        }
        return list;
    }

    public ChequeImpl(Context context) {
        super(context, Cheque.class);
    }

    @Override
    public Cheque get(Long pk) {
        Cheque cheque = null;
        try {
            cheque = getDao().queryForId(pk);
        } catch (SQLException e) {
            Log.e("SQLException", "Error during get Cheque by id", e);
        }
        return cheque;
    }
}
