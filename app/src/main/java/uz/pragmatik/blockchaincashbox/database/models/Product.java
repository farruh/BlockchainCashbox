package uz.pragmatik.blockchaincashbox.database.models;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;
import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Created by destbest on 18.04.2018.
 */

@DatabaseTable(tableName = "product")
public class Product implements Serializable {

  @DatabaseField(generatedId = true, columnName = "id")
  private Long id;

  @DatabaseField(columnName = "name")
  private String name;

  @DatabaseField(columnName = "price")
  private BigDecimal price;

  public Product() {
  }

  public Product(Long id, String name, BigDecimal price) {
    this.id = id;
    this.name = name;
    this.price = price;
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public BigDecimal getPrice() {
    return price;
  }

  public void setPrice(BigDecimal price) {
    this.price = price;
  }
}

