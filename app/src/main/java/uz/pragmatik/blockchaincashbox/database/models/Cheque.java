package uz.pragmatik.blockchaincashbox.database.models;

import com.j256.ormlite.dao.ForeignCollection;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import com.j256.ormlite.table.DatabaseTable;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by destbest on 18.04.2018.
 */

@DatabaseTable(tableName = "cheque")
public class Cheque implements Serializable {

    private static final long serialVersionUID = 1L;

//system
    @DatabaseField(columnName = "id", generatedId = true)
    private Long id;

	@DatabaseField(columnName = "cheque_sign")
    private Long chequeSign;
	
 //ui
    @DatabaseField(columnName = "cheque_number")
    private Long chequeNumber;

    @DatabaseField(columnName = "total_sum")
    private BigDecimal totalSum;

    @DatabaseField(columnName = "total_cash_sum")
    private BigDecimal totalCashSum;

    @DatabaseField(columnName = "total_ecash_sum")
    private BigDecimal totalEcashSum;

    @DatabaseField(columnName = "operation_date")
    private Date operationDate;

    @DatabaseField(columnName = "company_inn")
    private String companyInn;

    @DatabaseField(columnName = "company_name")
    private String companyName;

    @DatabaseField(columnName = "company_address")
    private String companyAddress;

    @ForeignCollectionField(columnName = "product_list")
    private ForeignCollection<ChequeProduct> chequeProducts;

    List<ChequeProduct> productList;

    public Cheque() {
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getChequeSign() {
        return chequeSign;
    }

    public void setChequeSign(Long chequeSign) {
        this.chequeSign = chequeSign;
    }

    public Long getChequeNumber() {
        return chequeNumber;
    }

    public void setChequeNumber(Long chequeNumber) {
        this.chequeNumber = chequeNumber;
    }

    public BigDecimal getTotalSum() {
        return totalSum;
    }

    public void setTotalSum(BigDecimal totalSum) {
        this.totalSum = totalSum;
    }

    public BigDecimal getTotalCashSum() {
        return totalCashSum;
    }

    public void setTotalCashSum(BigDecimal totalCashSum) {
        this.totalCashSum = totalCashSum;
    }

    public BigDecimal getTotalEcashSum() {
        return totalEcashSum;
    }

    public void setTotalEcashSum(BigDecimal totalEcashSum) {
        this.totalEcashSum = totalEcashSum;
    }

    public Date getOperationDate() {
        return operationDate;
    }

    public void setOperationDate(Date operationDate) {
        this.operationDate = operationDate;
    }

    public String getCompanyInn() {
        return companyInn;
    }

    public void setCompanyInn(String companyInn) {
        this.companyInn = companyInn;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
    }

    public List<ChequeProduct> getProductList() {
        return productList;
    }

    public void setProductList(List<ChequeProduct> productList) {
        this.productList = productList;
    }

    public List<ChequeProduct> getProductListFromDb(){
        List<ChequeProduct> list = new ArrayList<>();
        for(ChequeProduct chequeProduct : chequeProducts){
            list.add(chequeProduct);
        }
        return list;
    }
}
