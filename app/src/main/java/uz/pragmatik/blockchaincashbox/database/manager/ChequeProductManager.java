package uz.pragmatik.blockchaincashbox.database.manager;

import java.util.List;

import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;

/**
 * Created by destbest on 18.04.2018.
 */

public interface ChequeProductManager extends BaseManager<ChequeProduct, Long> {
    List<ChequeProduct> getById(Long id);
}
