package uz.pragmatik.blockchaincashbox.database.manager.impl;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import uz.pragmatik.blockchaincashbox.database.manager.ChequeProductManager;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;

/**
 * Created by destbest on 18.04.2018.
 */

public class ChequeProductImpl extends BaseManagerImpl<ChequeProduct, Long> implements ChequeProductManager {

    public ChequeProductImpl(Context context) {
        super(context, ChequeProduct.class);
    }

    @Override
    public List<ChequeProduct> getById(Long id) {
        List<ChequeProduct> list = null;
        try {
            list = getDao().queryForEq("cheque_id", id);
        } catch (SQLException e) {
            Log.e("SQLException", "Error during get cheque products by id", e);
        }

        return list;
    }
}
