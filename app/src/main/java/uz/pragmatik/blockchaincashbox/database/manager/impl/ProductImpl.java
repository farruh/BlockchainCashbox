package uz.pragmatik.blockchaincashbox.database.manager.impl;

import android.content.Context;
import android.util.Log;

import java.sql.SQLException;
import java.util.List;

import uz.pragmatik.blockchaincashbox.database.manager.ProductManager;
import uz.pragmatik.blockchaincashbox.database.models.Product;

/**
 * Created by destbest on 18.04.2018.
 */

public class ProductImpl extends BaseManagerImpl<Product, Long> implements ProductManager {

    public ProductImpl(Context context) {
        super(context, Product.class);
    }


    @Override
    public List<Product> getAll() {
        List<Product> list = null;
        try {
            list = getDao().queryForAll();
        } catch (SQLException e) {
            Log.e("SQLException", "Error during get all products", e);
        }
        return list;
    }

    @Override
    public Product get(Long pk) {
        Product product = null;
        try {
            product = getDao().queryForId(pk);
        } catch (SQLException e) {
            Log.e("SQLException", "Error during get product by id", e);
        }
        return product;
    }

    @Override
    public void delete(Long pk) {
        try {
            getDao().deleteById(pk);
        } catch (SQLException e) {
            Log.e("SQLException", "Error during delete product by id", e);
        }
    }
}
