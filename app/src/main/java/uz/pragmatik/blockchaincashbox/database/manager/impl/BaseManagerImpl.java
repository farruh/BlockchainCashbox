package uz.pragmatik.blockchaincashbox.database.manager.impl;

import android.content.Context;
import android.util.Log;

import com.j256.ormlite.android.apptools.OpenHelperManager;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.stmt.QueryBuilder;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import uz.pragmatik.blockchaincashbox.database.manager.BaseManager;
import uz.pragmatik.blockchaincashbox.database.manager.OrmLiteHelper;

/**
 * Created by destbest on 18.04.2018.
 */

public abstract class BaseManagerImpl<T, P> implements BaseManager<T, P> {
    private Context context;

    private OrmLiteHelper ormLiteHelper;
    private Dao<T, P> dao;

    public BaseManagerImpl(Context context, final Class<T> type) {
        this.context = context;
        this.ormLiteHelper = OpenHelperManager.getHelper(context, OrmLiteHelper.class);
        new ThrowWrapper() {
            @Override
            public void doSqlOperation() throws SQLException {
                dao = ormLiteHelper.getDao(type);
            }
        };
    }


    @Override
    public List<T> getAll() {
        final List<T> result = new ArrayList<T>();
        new ThrowWrapper() {
            @Override
            public void doSqlOperation() throws SQLException {
                result.addAll(dao.queryForAll());
            }
        };
        return result;

    }

    @Override
    public T get(final P pk) {
        final List<T> result = new ArrayList<T>();
        new ThrowWrapper() {
            @Override
            public void doSqlOperation() throws SQLException {
                result.add(dao.queryForId(pk));
            }
        };
        return !result.isEmpty() ? result.get(0) : null;
    }

    @Override
    public void save(final T object) {
        new ThrowWrapper() {
            @Override
            public void doSqlOperation() throws SQLException {
                dao.createOrUpdate(object);
            }
        };
    }

    @Override
    public void delete(final P pk) {
        new ThrowWrapper() {
            @Override
            public void doSqlOperation() throws SQLException {
                dao.delete(get(pk));
            }
        };
    }

    @Override
    public void clearAll() {
        throw new UnsupportedOperationException();
    }

    public Dao<T, P> getDao() {
        return dao;
    }

    abstract class ThrowWrapper {
        public ThrowWrapper() {
            try {
                doSqlOperation();
            } catch (SQLException e) {
                Log.e("SQLError", "Error during running database operation ", e);
            }
        }
        public abstract void doSqlOperation() throws SQLException;
    }

    protected T queryForFirst(Map<String, Object> parameters) {
        T result = null;
        if (!parameters.isEmpty()) {
            QueryBuilder<T, P> queryBuilder = getDao().queryBuilder();
            for (Map.Entry<String, Object> stringObjectEntry : parameters.entrySet()) {
                queryBuilder.where();//todo complete operation
            }
        }
        return result;
    }
}
