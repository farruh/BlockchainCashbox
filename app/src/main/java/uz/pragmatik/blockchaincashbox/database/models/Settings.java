package uz.pragmatik.blockchaincashbox.database.models;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by destbest on 18.04.2018.
 */

public class Settings {

    private SharedPreferences.Editor editor;
    private final String COMPANY_NAME = "companyName";
    private final String COMPANY_INN = "companyInn";
    private final String COMPANY_ADDRESS = "companyAddress";
    private final String SERVER_IP_ADDRESS = "serverIpAddress";

    private String companyName;

    private String companyInn;

    private String companyAddress;

    private String serverIpAddress;

    public Settings(Context context) {
        String prefName = "settings";
        SharedPreferences preferences = context.getSharedPreferences(prefName, MODE_PRIVATE);
        editor = preferences.edit();
        companyName = preferences.getString(COMPANY_NAME, "");
        companyInn = preferences.getString(COMPANY_INN, "");
        companyAddress = preferences.getString(COMPANY_ADDRESS, "");
        serverIpAddress = preferences.getString(SERVER_IP_ADDRESS, "");
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
        editor.putString(COMPANY_NAME, companyName);
        editor.commit();
    }

    public String getCompanyInn() {
        return companyInn;
    }

    public void setCompanyInn(String companyInn) {
        this.companyInn = companyInn;
        editor.putString(COMPANY_INN, companyInn);
        editor.commit();
    }

    public String getCompanyAddress() {
        return companyAddress;
    }

    public void setCompanyAddress(String companyAddress) {
        this.companyAddress = companyAddress;
        editor.putString(COMPANY_ADDRESS, companyAddress);
        editor.commit();
    }

    public String getServerIpAddress() {
        return serverIpAddress;
    }

    public void setServerIpAddress(String serverIpAddress) {
        this.serverIpAddress = serverIpAddress;
        editor.putString(SERVER_IP_ADDRESS, serverIpAddress);
        editor.commit();
    }
}
