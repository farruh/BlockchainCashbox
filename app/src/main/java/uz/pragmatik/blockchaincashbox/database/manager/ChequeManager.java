package uz.pragmatik.blockchaincashbox.database.manager;

import uz.pragmatik.blockchaincashbox.database.models.Cheque;

/**
 * Created by destbest on 18.04.2018.
 */

public interface ChequeManager extends BaseManager<Cheque, Long>{

}
