package uz.pragmatik.blockchaincashbox.database.manager;

import uz.pragmatik.blockchaincashbox.database.models.Product;

/**
 * Created by destbest on 18.04.2018.
 */

public interface ProductManager extends BaseManager<Product, Long> {

}
