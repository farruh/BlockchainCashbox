package uz.pragmatik.blockchaincashbox;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.journeyapps.barcodescanner.BarcodeEncoder;

import uz.pragmatik.blockchaincashbox.utils.Constants;

public class ChequeActivity extends AppCompatActivity {

    Button btnNewSelling;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cheque);
        btnNewSelling = findViewById(R.id.btn_new_selling);
        btnNewSelling.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        createQrCode();
    }

    private void createQrCode() {
        try {

            Intent intent = getIntent();
            Long chequeId = intent.getLongExtra(Constants.CHEQUE_ID,-1);
            Toast.makeText(this, "chequeId: " + chequeId, Toast.LENGTH_LONG).show();
//
            if (chequeId != null && chequeId>0) {
                BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
                Bitmap bitmap = barcodeEncoder.encodeBitmap(chequeId.toString(), BarcodeFormat.QR_CODE, 500, 500);
                ImageView imageViewQrCode = (ImageView) findViewById(R.id.qr_code);
                imageViewQrCode.setImageBitmap(bitmap);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, CashboxActivity.class);
        startActivity(intent);
    }
}
