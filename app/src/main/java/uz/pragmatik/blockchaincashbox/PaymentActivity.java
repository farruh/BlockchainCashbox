package uz.pragmatik.blockchaincashbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.math.BigDecimal;

import uz.pragmatik.blockchaincashbox.adapters.PaymentsAdapter;
import uz.pragmatik.blockchaincashbox.database.ChequeHelper;
import uz.pragmatik.blockchaincashbox.database.models.Cheque;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.fragments.SellingFragment;
import uz.pragmatik.blockchaincashbox.utils.Constants;

public class PaymentActivity extends AppCompatActivity implements View.OnClickListener {

    private RecyclerView rvPayment;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    Button btnCard;
    Button btnCash;
    BigDecimal totalSum;
    TextView tvTotal;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tvTotal = findViewById(R.id.tv_total);
        btnCard = findViewById(R.id.btn_card);
        btnCash = findViewById(R.id.btn_cash);

        btnCard.setOnClickListener(this);
        btnCash.setOnClickListener(this);

        rvPayment = findViewById(R.id.rv_payment);
        rvPayment.setHasFixedSize(true);

        mLayoutManager = new LinearLayoutManager(this);
        rvPayment.setLayoutManager(mLayoutManager);

        mAdapter = new PaymentsAdapter(SellingFragment.chequeProductsList, this);
        rvPayment.setAdapter(mAdapter);

        totalSum = BigDecimal.ZERO;
        for (ChequeProduct product: SellingFragment.chequeProductsList){
            totalSum = totalSum.add(product.getPrice().multiply(product.getQuantity()));
        }
        tvTotal.setText(totalSum.toString());


    }

    @Override
    public void onClick(View v) {
        Cheque cheque = new Cheque();
        ChequeHelper chequeHelper = new ChequeHelper(this);
        cheque.setTotalSum(totalSum);
        cheque.setProductList(SellingFragment.chequeProductsList);
        int i = v.getId();
        if (i == R.id.btn_card || i == R.id.btn_cash) {
            if (i == R.id.btn_card ) {
                cheque.setTotalEcashSum(totalSum);
            }else{
                cheque.setTotalCashSum(totalSum);
            }
            chequeHelper.setCheque(cheque);
            Intent intentCard = new Intent(this, ChequeActivity.class);
            intentCard.putExtra(Constants.CHEQUE_ID, cheque.getChequeNumber());
            startActivity(intentCard);

        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;

    }
}
