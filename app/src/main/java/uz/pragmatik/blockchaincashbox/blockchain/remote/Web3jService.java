package uz.pragmatik.blockchaincashbox.blockchain.remote;

import android.util.Log;
import java.io.IOException;
import java.math.BigInteger;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.Web3jFactory;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.ClientTransactionManager;
import org.web3j.tx.Contract;
import org.web3j.tx.ManagedTransaction;
import uz.pragmatik.blockchaincashbox.blockchain.wrapper.ChequeHolder;
import uz.pragmatik.blockchaincashbox.database.models.Cheque;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;

/**
 * @author Farruh Tashbulov
 */

public class Web3jService {
  static private final String CONTRACT_ADDRESS = "0xa9d59474b35681c30573da96d0923ed51eadb3f0";
  static private final String ACCOUNT_PUB_KEY = "0xf7Ff2e238E9a830cF5043484DA62162831b97d4C";
  static private final String ACCOUNT_PRV_KEY =
      "d2910d2ddbda1fac5a4598cb13f99a20c453e6d423dcc1b3f1cabfeb03956c63";

  static private final String hexPublicKey =
      String.format("%040x", new BigInteger(1, ACCOUNT_PUB_KEY.getBytes()));
  static private final String hexPrivateKey =
      String.format("%040x", new BigInteger(1, ACCOUNT_PRV_KEY.getBytes()));

  private ChequeHolder chequeHolder;
  private String serverIpAddress;
  public Web3jService(String serverIpAddress) {
    try {
      this.serverIpAddress = serverIpAddress;
      if (serverIpAddress == null || serverIpAddress.isEmpty()) {
        throw new IllegalStateException("There is no server IP setted for web3j.");
      }
      Web3j web3j = Web3jFactory.build(
          new HttpService(serverIpAddress));  // FIXME: Enter your Infura token here;

      Log.i("blockchain", "Connected to Ethereum client version: " + web3j.web3ClientVersion()
          .send()
          .getWeb3ClientVersion());
      ClientTransactionManager clientTransactionManager =
          new ClientTransactionManager(web3j, ACCOUNT_PUB_KEY);
      //Credentials credentials = Credentials.create(ACCOUNT_PRV_KEY, ACCOUNT_PUB_KEY);
      chequeHolder = ChequeHolder.load(CONTRACT_ADDRESS, web3j, clientTransactionManager,
          ManagedTransaction.GAS_PRICE, Contract.GAS_LIMIT);
      Log.i("blockchain", "Connected to network");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void sendCheque(Cheque cheque) {

    if (chequeHolder != null) {//if initialized
      try {
        TransactionReceipt receipt = chequeHolder.addCheqeu(
            cheque.getChequeNumber() != null ? BigInteger.valueOf(cheque.getChequeNumber())
                : BigInteger.ZERO,
            cheque.getChequeSign() != null ? BigInteger.valueOf(cheque.getChequeSign())
                : BigInteger.ZERO, cheque.getTotalSum().toBigInteger(),//fixme: convert to integer
            cheque.getTotalCashSum() != null ? cheque.getTotalCashSum().toBigInteger()
                : BigInteger.ZERO,//fixme: convert to integer
            cheque.getTotalEcashSum() != null ? cheque.getTotalEcashSum().toBigInteger()
                : BigInteger.ZERO,//fixme: convert to integer
            cheque.getCompanyInn() != null ? cheque.getCompanyInn() : "",
            cheque.getCompanyName() != null ? cheque.getCompanyName() : "",
            cheque.getCompanyAddress() != null ? cheque.getCompanyAddress() : "").send();
        Log.d("transaction", "" + receipt);
      } catch (Exception e) {
        e.printStackTrace();
      }

      for (ChequeProduct chequeProduct : cheque.getProductList()) {
        try {
          TransactionReceipt receipt = chequeHolder.addChequeProduct(
              cheque.getChequeNumber() != null ? BigInteger.valueOf(cheque.getChequeNumber())
                  : BigInteger.ZERO, chequeProduct.getName(),
              chequeProduct.getPrice().toBigInteger(),
              //fixme: convert to integer
              chequeProduct.getPrice() != null ? chequeProduct.getPrice().toBigInteger()
                  : BigInteger.ZERO,// fixme: convert to integer
              chequeProduct.getTotalSum() != null ? chequeProduct.getTotalSum().toBigInteger()
                  : BigInteger.ZERO).send();
          Log.d("transaction", "" + receipt);
        } catch (Exception e) {
          e.printStackTrace();
        }
      }
    }
  }
}
