package uz.pragmatik.blockchaincashbox;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.math.BigDecimal;
import java.util.List;

import uz.pragmatik.blockchaincashbox.database.manager.ProductManager;
import uz.pragmatik.blockchaincashbox.database.manager.impl.ProductImpl;
import uz.pragmatik.blockchaincashbox.database.models.Product;
import uz.pragmatik.blockchaincashbox.utils.Constants;

public class AddProductActivity extends AppCompatActivity {

    ProductManager manager;
    List<Product> productList;
    private EditText etName;
    private EditText etPrice;
    private Long productId = Long.valueOf(-1);
    Button btnDelete;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_form);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        etName = findViewById(R.id.et_name);
        etPrice = findViewById(R.id.et_price);
        manager = new ProductImpl(this);
        btnDelete = findViewById(R.id.btn_delete_product);
        Intent intent = getIntent();
        productId = intent.getLongExtra(Constants.PRODUCT_ID, -1);
        if (productId!=-1){
            btnDelete.setVisibility(View.VISIBLE);
            etName.setText(manager.get(productId).getName());
            etPrice.setText(manager.get(productId).getPrice().toString());
        }
        else
        {
            btnDelete.setVisibility(View.GONE);
        }

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                manager.delete(productId);
                onBackPressed();
            }
        });
        manager = new ProductImpl(this);

    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_save, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_save:
                boolean error = false;

                if (!(etName!=null || etName.getText().length()>0)){
                    etName.setError(getResources().getString(R.string.empty_error));
                    error = true;
                } else {
                    etName.setError(null);
                }
                if (!(etPrice==null || etPrice.getText().length()>0)){
                    etPrice.setError(getResources().getString(R.string.empty_error));
                    error = true;
                } else {
                    try {
                        new BigDecimal(etPrice.getText().toString());
                        etPrice.setError(null);
                    } catch (Exception e){
                        etPrice.setError(getResources().getString(R.string.format_error));
                        error = true;
                    }

                }
                if (!error){
                    saveProduct();
                }

                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }
        return true;

    }

    private void saveProduct() {
        if (productId==-1) {
            Product product = new Product();
            product.setName(etName.getText().toString());
            product.setPrice(new BigDecimal(etPrice.getText().toString()));
            manager.save(product);
        } else {
            Product product = manager.get(productId);
            product.setName(etName.getText().toString());
            product.setPrice(new BigDecimal(etPrice.getText().toString()));
            manager.save(product);
        }
        onBackPressed();
    }
}
