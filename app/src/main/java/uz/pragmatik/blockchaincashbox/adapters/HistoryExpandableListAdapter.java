package uz.pragmatik.blockchaincashbox.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.database.models.ChequeHeader;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class HistoryExpandableListAdapter extends BaseExpandableListAdapter {

    private Context _context;
    private List<ChequeHeader> _listDataHeader; // header titles
    // child data in format of header title, child title
    private HashMap<ChequeHeader, List<ChequeProduct>> _listDataChild;

    public HistoryExpandableListAdapter(Context context, List<ChequeHeader> listDataHeader,
                                        HashMap<ChequeHeader, List<ChequeProduct>> listChildData) {
        this._context = context;
        this._listDataHeader = listDataHeader;
        this._listDataChild = listChildData;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .get(childPosititon).getName();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.history_list_item, null);
        }

        TextView tvId = convertView.findViewById(R.id.tv_id);
        TextView tvName = convertView.findViewById(R.id.tv_name);
        TextView tvAmount = convertView.findViewById(R.id.tv_amount);
        TextView tvTotalSum = convertView.findViewById(R.id.tv_price);
        tvId.setText(String.valueOf(childPosition+1));
        tvName.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getName().toString());
        tvAmount.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getQuantity().toString());
        tvTotalSum.setText(_listDataChild.get(_listDataHeader.get(groupPosition)).get(childPosition).getTotalSum().toString());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition))
                .size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition).getChequeLabel();
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.history_list_group, null);
        }

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setText((String)getGroup(groupPosition));
        TextView tvTotalSum = convertView.findViewById(R.id.tv_cheque_total);
        tvTotalSum.setText(this._listDataHeader.get(groupPosition).getChequeTotalSum()+" ("+this._listDataHeader.get(groupPosition).getPaymentType()+")");

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}