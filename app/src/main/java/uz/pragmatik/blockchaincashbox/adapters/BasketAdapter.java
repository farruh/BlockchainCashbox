package uz.pragmatik.blockchaincashbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.math.BigDecimal;
import java.util.List;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.fragments.SellingFragment;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class BasketAdapter extends RecyclerView.Adapter<BasketAdapter.ViewHolder> {


    private Context context;

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvId;
        public TextView tvName;
        public TextView tvPrice;
        public EditText etAmount;
        public ImageView ivIncrease;
        public ImageView ivDecrease;
        public int currentPosition;
        public LinearLayout llRow;

        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_id);
            tvName = view.findViewById(R.id.tv_name);
            tvPrice = view.findViewById(R.id.tv_price);
            etAmount = view.findViewById(R.id.et_amount);
            ivIncrease = view.findViewById(R.id.iv_increase);
            ivDecrease = view.findViewById(R.id.iv_decrease);
            llRow = view.findViewById(R.id.ll_row);

            ivIncrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    etAmount.setText(String.valueOf(Long.parseLong(etAmount.getText().toString())+1));
                }
            });
            ivDecrease.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Long.valueOf(etAmount.getText().toString())>1L){
                        etAmount.setText(String.valueOf(Long.parseLong(etAmount.getText().toString())-1));
                    }
                }
            });

            etAmount.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    try {
                        SellingFragment.chequeProductsList.get(currentPosition).setQuantity(new BigDecimal(s.toString()));
                        SellingFragment.chequeProductsList.get(currentPosition).setTotalSum(new BigDecimal(s.toString()).multiply(SellingFragment.chequeProductsList.get(currentPosition).getPrice()));
                    }
                    catch (Exception e){
                        etAmount.setText("1");
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });

        }
    }

    public BasketAdapter(List<ChequeProduct> chequeProductsList, Context context) {
        this.context = context;
    }

    @Override
    public BasketAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_reciept, parent, false);
        BasketAdapter.ViewHolder vh = new BasketAdapter.ViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(BasketAdapter.ViewHolder holder, int position) {
        holder.tvId.setText(String.valueOf(position+1));
        holder.tvName.setText(SellingFragment.chequeProductsList.get(position).getName());
        holder.tvPrice.setText(String.valueOf(SellingFragment.chequeProductsList.get(position).getPrice()));
        holder.etAmount.setText(SellingFragment.chequeProductsList.get(position).getQuantity().toString());
        holder.currentPosition = position;
        if (position%2==0) {
            holder.llRow.setBackgroundColor(context.getResources().getColor(R.color.listRowColor));
        }

    }

    @Override
    public int getItemCount() {
        return SellingFragment.chequeProductsList.size();
    }
}