package uz.pragmatik.blockchaincashbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.database.models.Product;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {

    private List<Product> productList;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvId;
        public TextView tvName;
        public TextView tvPrice;
        public LinearLayout llRow;
        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_id);
            tvName = view.findViewById(R.id.tv_name);
            tvPrice = view.findViewById(R.id.tv_price);
            llRow = view.findViewById(R.id.ll_row);

        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductsAdapter(List<Product> productList, Context context) {
       this.productList = productList;
       this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                   int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_products, parent, false);
        ViewHolder vh = new ViewHolder(view);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvId.setText(String.valueOf(productList.get(position).getId()));
        holder.tvName.setText(productList.get(position).getName());
        holder.tvPrice.setText(String.valueOf(productList.get(position).getPrice()));
        if (position%2==0) {
            holder.llRow.setBackgroundColor(context.getResources().getColor(R.color.listRowColor));
        }

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return productList.size();
    }
}