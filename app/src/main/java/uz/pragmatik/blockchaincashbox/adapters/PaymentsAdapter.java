package uz.pragmatik.blockchaincashbox.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import uz.pragmatik.blockchaincashbox.R;
import uz.pragmatik.blockchaincashbox.database.models.ChequeProduct;
import uz.pragmatik.blockchaincashbox.fragments.SellingFragment;

/**
 * Created by QarakenBacho on 18.04.2018.
 */

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    private List<ChequeProduct> paymentList;
    private Context context;


    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView tvId;
        public TextView tvName;
        public TextView tvPrice;
        public TextView tvAmount;
        public LinearLayout llRow;

        public ViewHolder(View view) {
            super(view);
            tvId = view.findViewById(R.id.tv_id);
            tvName = view.findViewById(R.id.tv_name);
            tvPrice = view.findViewById(R.id.tv_price);
            tvAmount = view.findViewById(R.id.tv_amount);
            llRow = view.findViewById(R.id.ll_row);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public PaymentsAdapter(List<ChequeProduct> paymentList, Context context) {
        this.paymentList = paymentList;
        this.context = context;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public PaymentsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        // create a new view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_payment, parent, false);
        PaymentsAdapter.ViewHolder vh = new PaymentsAdapter.ViewHolder(view);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(PaymentsAdapter.ViewHolder holder, int position) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        holder.tvId.setText(String.valueOf(position+1));
        holder.tvName.setText(SellingFragment.chequeProductsList.get(position).getName());
        holder.tvPrice.setText(String.valueOf(SellingFragment.chequeProductsList.get(position).getTotalSum()));
        holder.tvAmount.setText(String.valueOf(SellingFragment.chequeProductsList.get(position).getQuantity()));
        if (position%2==0) {
            holder.llRow.setBackgroundColor(context.getResources().getColor(R.color.listRowColor));
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return SellingFragment.chequeProductsList.size();
    }
}