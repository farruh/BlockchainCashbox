package com.pragmatik.chequetokenwallet;

import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.integration.android.IntentResult;
import com.journeyapps.barcodescanner.BarcodeEncoder;

public class Dashboard extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(uz.pragmatik.chequetokenwallet.R.layout.activity_dashboard);
        setQrCode();
    }
    private void setQrCode(){
        try {
            BarcodeEncoder barcodeEncoder = new BarcodeEncoder();
            Bitmap bitmap = barcodeEncoder.encodeBitmap("content", BarcodeFormat.QR_CODE, 500, 500);
            ImageView imageViewQrCode = (ImageView) findViewById(uz.pragmatik.chequetokenwallet.R.id.qr_code);
            imageViewQrCode.setImageBitmap(bitmap);
        } catch(Exception e) {

        }
    }
    @Override
    public void onClick(View v) {
        switch (v.getId())
        {
            case uz.pragmatik.chequetokenwallet.R.id.btn_new_selling:
//                Intent intentCard = new Intent(this, BarcodeActivityHelper.class);
                new BarcodeActivityHelper().showBarcodeReaderActivity(this);
//                startActivity(intentCard);
                break;
        }
    }
    // Get the results:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (BarcodeActivityHelper.BARCODE_ACTIVITY_REQUEST_CODE == requestCode) {
            IntentResult result = new BarcodeActivityHelper().getBarcodeResult(requestCode, resultCode, data);
            if (result != null) {
                if (result.getContents() == null) {
                    Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
//                    showFormByBarCode(result.getContents());
                }
            }
        }
         else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
