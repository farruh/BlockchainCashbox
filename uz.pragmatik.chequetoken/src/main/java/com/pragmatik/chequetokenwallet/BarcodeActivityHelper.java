package com.pragmatik.chequetokenwallet;

import android.app.Activity;
import android.content.Intent;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import static com.google.zxing.integration.android.IntentIntegrator.REQUEST_CODE;

/**
 * @author Farruh Tashbulov
 */

public class BarcodeActivityHelper {
    public static final int BARCODE_ACTIVITY_REQUEST_CODE = REQUEST_CODE;

    public void showBarcodeReaderActivity(Activity source) {
        IntentIntegrator integrator = new IntentIntegrator(source);
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE);
        integrator.setPrompt(source.getResources().getString(uz.pragmatik.chequetokenwallet.R.string.lblBarCodeScan));
        integrator.setBarcodeImageEnabled(false);
        integrator.setOrientationLocked(false);
        integrator.initiateScan();
    }

    public IntentResult getBarcodeResult(int requestCode, int resultCode, Intent intent) {
        return IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);
    }
}
